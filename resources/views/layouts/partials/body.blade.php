@include('layouts.partials.head')
@include('layouts.partials.navbar')
@include('layouts.partials.sidebar')

@yield('content')

@include('layouts.partials.script')

