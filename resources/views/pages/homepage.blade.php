@extends('layouts.partials.body')
@section('content')

<div>
    @include('layouts.partials.agenda')
    @include('layouts.partials.berita')
    @include('layouts.partials.footer')

</div>
@endsection